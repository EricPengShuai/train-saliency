## Pytorch提取显著性
**master: transfer VGG-16 model parameters**

1. 模型架构参考`convnet.py`
2. 训练过程参考`trainSaliency.py`
3. 相关参数参考`Argument.py`
4. loss function: `loss.py`

> 使用的数据是采样之后的`./framed/sample`文件夹作为inputs，然后是attention采样之后的显著性`D:/VR_project/PanoSaliency/data/dataset2.npy`作为labels

## Log

- **[1217]**：训练两个deepCNN，测试三个指标：CC、NSS、KL
  1. 训练好的模型文件夹：`model_pth`
  2. 训练之后显著性输出：`outputs_sal_maps`
  

## Problem

1. 训练存在比较多的相似帧，但是此时的label有点不一样，让模型学习不到东西
<img src="https://i.loli.net/2021/07/31/5t29lyGOgri71bw.png" alt="相似帧" style="zoom: 70%;" />


## Result

1. MSE loss function: not good
2. KL loss function: not bad

