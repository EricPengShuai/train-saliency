from argparse import ArgumentParser
# sal_path = 'D:/VR_project/PanoSaliency/data/dataset2.npy'
sal_path = 'D:/VR_project/saliency-convnet-7.31/sample_data/dataset2.npy'

# image_path = './framed/sample'
image_path = './sample_frames/'

parser = ArgumentParser(description = 'Saliency Detection')
parser.add_argument('--batch_size', default=8, type=int, help='batch size for dataloader')
parser.add_argument('--learning_rate', default=1e-5, type=float, help='learning rate of model')
parser.add_argument('--epoches', default=50, type=int, help='train epoch of model')
parser.add_argument('--decay', default=0, type=float, help='weight_decay of model')

# path setting
parser.add_argument('--IsTrain', default=False, type=bool, help='train or test')
parser.add_argument('--IsShow', default=False, type=bool, help='show image or not')
parser.add_argument('--suffle', default=False, type=bool, help='suffle or not')
parser.add_argument('--sal_path', default=sal_path,
                    type=str, help='saliency data path')
parser.add_argument('--model_path', default='./model_pth/deepCNN/model1.pth', type=str, help='model path')
parser.add_argument('--image_path', default=image_path, type=str, help='RGB sample frame dir')
parser.add_argument('--ratio', default=0.9, type=float, help='ratio of train to test')
parser.add_argument('--save_path',default='./output_sal_maps/dcnn1.npy', type=str, help='save output saliency maps')

def get_args():
    args = parser.parse_args()
    return args

# '--sal_path', default='D:/VR_project/PanoSaliency/data/dataset2.npy',
# '--image_path', default='./framed/sample'

# '--image_path', default='D:/360 dataset/360_Saliency_dataset_2018ECCV/sample_data/
# '--sal_path', default='D:/360 dataset/360_Saliency_dataset_2018ECCV/sample_data.npy',